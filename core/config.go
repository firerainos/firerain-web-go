package core

import (
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"os"
)

var Conf *Config

type Config struct {
	Db          Database      `json:"database"`
	Redis       redis.Options `json:"redis"`
	Smtp        Smtp          `json:"smtp"`
	GoogleCloud GoogleCloud   `json:"googleCloud"`
}

type Database struct {
	Driver   string `json:"driver"`
	Address  string `json:"address" form:"address"`
	Port     string `json:"port" form:"port"`
	Dbname   string `json:"dbname" form:"dbname"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type Smtp struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Host     string `json:"host"`
}

type GoogleCloud struct {
	Credentials json.RawMessage `json:"credentials"`
}

func ParseConf(config string) error {
	var c Config

	conf, err := os.Open(config)
	if err != nil {
		return err
	}
	err = json.NewDecoder(conf).Decode(&c)

	Conf = &c
	return err
}
