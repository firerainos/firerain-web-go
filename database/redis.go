package database

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/firerainos/firerain-web-go/core"
	"sync"
)

var (
	redisOnce   sync.Once
	redisClient *redis.Client
)

func Redis() *redis.Client {
	redisOnce.Do(func() {
		redisClient = redis.NewClient(&core.Conf.Redis)
	})

	return redisClient
}
