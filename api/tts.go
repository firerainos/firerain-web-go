package api

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/firerainos/firerain-web-go/database"
	"gitlab.com/firerainos/firerain-web-go/tts"
	"log"
	"strings"
	"time"
)

func TextToSpeech(ctx *gin.Context) {
	if !strings.HasPrefix(ctx.Request.UserAgent(), "FireRain Installer") {
		ctx.String(403, "Forbidden")
		return
	}

	text := ctx.Query("text")
	if text == "" {
		ctx.String(400, "text not found")
		return
	}

	key := ctx.Param("language") + "." + text

	redis := database.Redis()
	if bytes, err := redis.Get(context.TODO(), key).Bytes(); err == nil {
		log.Println("get voice from redis cache, [key]:", key)

		ctx.Header("Content-Disposition", "attachment; filename=voice.mp3")
		ctx.Data(200, "audio/mpeg", bytes)
		return
	}

	bytes, err := tts.GetSpeechWithText(text, ctx.Param("language"))
	if err != nil {
		ctx.String(500, err.Error())
	} else {
		if err := redis.Set(context.TODO(), key, bytes, time.Hour*72).Err(); err != nil {
			log.Println("save voice to redis cache fail, [key]:", key, "[error]: ", err)
		}

		ctx.Header("Content-Disposition", "attachment; filename=voice.mp3")
		ctx.Data(200, "audio/mpeg", bytes)
	}
}
