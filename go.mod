module gitlab.com/firerainos/firerain-web-go

go 1.16

require (
	cloud.google.com/go v0.80.0
	github.com/PuerkitoBio/goquery v1.4.1
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/denisenkom/go-mssqldb v0.9.0 // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis/v8 v8.11.0
	github.com/go-sql-driver/mysql v1.4.0 // indirect
	github.com/jinzhu/gorm v1.9.1
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	google.golang.org/api v0.43.0
	google.golang.org/genproto v0.0.0-20210323160006-e668133fea6a
)
