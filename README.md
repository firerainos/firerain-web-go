# firerain-web-go
> firerain官网后端

[![pipeline status](https://gitlab.com/firerainos/firerain-web-go/badges/master/pipeline.svg)](https://gitlab.com/firerainos/firerain-web-go/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/firerainos/firerain-web-go)](https://goreportcard.com/report/gitlab.com/firerainos/firerain-web-go)
[![GoDoc](https://godoc.org/gitlab.com/firerainos/firerain-web-go?status.svg)](https://godoc.org/gitlab.com/firerainos/firerain-web-go)
[![Sourcegraph](https://sourcegraph.com/gitlab.com/firerainos/firerain-web-go/-/badge.svg)](https://sourcegraph.com/gitlab.com/firerainos/firerain-web-go)

# Install
- go get gitlab.com/firerainos/firerain-web-go
- cd your GOPATH/gitlab.com/firerainos/firerain-web-go
- go build main.go
- cp config.default.json config.json
- edit config.json

# Boot
`main [-p port]`

`./main` 

or 

`./main -p 80`
