package tts

import (
	tts "cloud.google.com/go/texttospeech/apiv1"
	"context"
	"gitlab.com/firerainos/firerain-web-go/core"
	"google.golang.org/api/option"
	ttspb "google.golang.org/genproto/googleapis/cloud/texttospeech/v1"
)

var Client *tts.Client

func InitClient() error {
	var err error
	Client, err = tts.NewClient(context.Background(), option.WithCredentialsJSON(core.Conf.GoogleCloud.Credentials))
	if err != nil {
		return err
	}

	return nil
}

func GetSpeechWithText(text, language string) ([]byte, error) {
	req := ttspb.SynthesizeSpeechRequest{
		Input: &ttspb.SynthesisInput{
			InputSource: &ttspb.SynthesisInput_Text{Text: text},
		},
		Voice: &ttspb.VoiceSelectionParams{
			LanguageCode: language,
			SsmlGender:   ttspb.SsmlVoiceGender_NEUTRAL,
		},
		AudioConfig: &ttspb.AudioConfig{
			AudioEncoding: ttspb.AudioEncoding_MP3,
		},
	}

	resp, err := Client.SynthesizeSpeech(context.Background(), &req)
	if err != nil {
		return nil, err
	}

	return resp.AudioContent, nil
}
